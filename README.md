# container deployment
What it does:
- Example web site hosted via container deployed to dev, staging, and prod. Instead of using different servers, three different ports are used on the same target host. See screen shot of result.

Setup:
- Setup of shell and docker runners.
- SSH key creation and configuration on hosts. 
- Declaring variables in the .gitlab-ci.yml file.
- Control of caching with pull-push.

Sequence:
- Build docker image providing name and version. 
- Push docker image to gitlab container registry.
- Use of "extends" to declare a block of code for re-use by other jobs. 
- Deploy image from container registry to dev and staging servers to host the app/site.
- Use of "when" to pause pipeline for approval before pushing into prod.
